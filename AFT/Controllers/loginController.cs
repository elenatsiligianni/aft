﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AFT.Controllers
{
    public class loginController : Controller
    {
        //
        // GET: /login/
        
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Username == "AFT-NIKIFOROS")
                {
                    
                    FormsAuthentication.SetAuthCookie(model.Username,false);
                    return RedirectToAction("index", "home"); //index page of the home controller
                    
                }
                {
                    ModelState.AddModelError("", "Invalid username");
                }
            }
            return View();
        }

    }
}
