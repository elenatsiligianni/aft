﻿using AFT.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AFT.Controllers
{
    
    public class homeController : Controller
    {

        string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/bookings.csv");
        // GET: /home/
    
        public ActionResult Index()
        {

             BookingsModel model = new BookingsModel();
             model.listBookings = GetBookings();
             return View(model);

        }
     
        private List<BookingsModel> GetBookings()
        {
            List<BookingsModel> list = new List<BookingsModel>();
            using (StreamReader sr = new StreamReader(FilePath))
            {
                string currentLine = string.Empty;
                while ((currentLine = sr.ReadLine()) != null)
                {
                
                        string[] row = new string[7];
                        row = currentLine.Split(',');
                        list.Add(new BookingsModel{ Agent_name = row[0], Booking_id = row[1], Trav_Full_name = row[2],
                                                     Product_booked = row[3],Origin = row[4],Destination = row[5],From = row[6],To=row[7]
                        });
                  
                }
            }
            return list;
        }  

    }
}
