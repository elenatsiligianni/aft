﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFT.Models
{
    public class BookingsModel
    {
        public List<BookingsModel> listBookings { get; set; }
        public string Agent_name { get; set; }
        public string Booking_id { get; set; }
        public string Trav_Full_name { get; set; }
        public string Product_booked { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string From { get; set; }
        public string To { get; set; }

    }
}